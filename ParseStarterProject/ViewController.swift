//
//  ViewController.swift
//
//  Copyright 2011-present Parse Inc. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        testParseSaveIssue()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func testParseSaveIssue() {
        
        let str = "Working at Parse is great!"
        let data = str.dataUsingEncoding(NSUTF8StringEncoding)
        let file = PFFile(name:"resume.txt", data:data!)
        
        file.saveInBackgroundWithBlock({ (succeeded: Bool, error: NSError?) -> Void in
            // Handle success or failure here ...
            if succeeded {
                println("Save successful")
            } else {
                println("Save unsuccessful: \(error?.userInfo)")
            }
            
            }, progressBlock: { (percentDone: Int32) -> Void in
            // Update your progress spinner here. percentDone will be between 0 and 100.
        })
        
    }
}

